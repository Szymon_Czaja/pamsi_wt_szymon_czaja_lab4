#pragma once

#ifndef _WEZEL_  
#define _WEZEL_  

#include <string>

template<typename E>
class wezel
{
private:
	E element;
	wezel<E>* next_P;
	wezel<E>* next_L;
	wezel<E>* back;
	int sons;
public:
	wezel()
	{
		next_P = NULL;
		next_L = NULL;
		back = NULL;
		sons = 0;
	}
	

	E get_element()
	{
		return element;
	}

	void set_element(E elem)
	{
		element = elem;
	}

	wezel<E>* get_next_P()
	{
		return next_P;
	}

	wezel<E>* get_next_L()
	{
		return next_L;
	}

	void set_next_P(wezel<E> *next)
	{
		next_P = next;
	}

	void set_next_L(wezel<E> *next)
	{
		next_L = next;
	}

	wezel<E>* get_back()
	{
		return back;
	}

	void set_back(wezel<E>* poprzedni)
	{
		back = poprzedni;
	}

	int get_sons_count()
	{
		return sons;
	}

	void set_sons_count(int i)
	{
		sons = i;
	}
};
#endif 