
#include "drzewo_binarne.h"
#include <iostream>

using namespace std;


int main()
{
	try
	{
		drzewo_binarne <int> d;
		d.get_head()->set_element(9);
	
		d.add_son_P(1, d.get_head());
		d.add_son_L(2, d.get_head());
		d.add_son_P(3, d.get_head()->get_next_P());
		d.add_son_L(4, d.get_head()->get_next_P()->get_next_P());
		d.add_son_P(5, d.get_head()->get_next_P()->get_next_P());
		d.add_son_L(6, d.get_head()->get_next_P()->get_next_P()->get_next_L());
		d.add_son_P(7, d.get_head()->get_next_P()->get_next_P()->get_next_L());
		cout << "ilosc: " << d.size() << endl;
		d.print_in();
		//d.remove_son_L(d.get_head()->get_next_P()->get_next_P());
		//d.remove_son_L(d.get_head()->get_next_P()->get_next_P());
		cout << "ilosc: " << d.size() << endl;
		d.print_pre();
		cout << "ilosc: " << d.size() << endl;
		//cout << "wysokosc: " << d.height(d.get_head()) << endl;
		d.print_post();
	}
	catch (string w)
	{
		cout << "Wyjatek: " << w << endl;
	}
	system("PAUSE");
}