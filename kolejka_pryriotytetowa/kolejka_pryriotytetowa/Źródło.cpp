
#include "kolejka_priorytetowa.h"

template<typename E>
void sortowanie(Deque_p<E> &sort)
{
	Deque <E> tmp;
	//sort.print();
	while (!sort.empty())
	{
		tmp.add_front(sort.remove_min());
	} 
	//tmp.print();
	while (!tmp.empty())
	{
		sort.add_front(tmp.front());
	}
}


int main()
{
	try
	{
		Deque_p <int> sort;
		sort.insert(2, 2);
		sort.insert(4, 4);
		sort.insert(6, 6);
		sort.insert(8, 8);
		sort.insert(1, 1);
		sort.insert(34, 34);
		sort.insert(35, 35);
		sort.insert(8, 8);
		sort.insert(6, 6);
		sort.insert(4, 4);
		sort.insert(12, 12);
		sort.insert(83, 83);
		sort.insert(10, 10);
		sort.insert(3, 3);
		sort.insert(5, 5);
		sort.insert(84, 84);

		cout << "NIE POSORTOWANE: " << endl;
		sort.print();
		sortowanie(sort);
		cout <<"POSORTOWANE: "<< endl ;
		sort.print();

	}
	catch (string w)
	{
		cout << "Wyjatek: " << w << endl;
	}

	system("PAUSE");
	return 0;
}