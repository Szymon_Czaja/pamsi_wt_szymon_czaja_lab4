
#include "drzewo.h"
#include <iostream>

using namespace std;


int main()
{
	try
	{
		drzewo <int> d;
		d.get_head()->set_element(9);
		d.add_son(1, d.get_head());
		d.add_son(2, d.get_head());
		d.add_son(31, d.get_head()->get_next(1));
		d.add_son(35, d.get_head()->get_next(1));
		d.add_son(36, d.get_head()->get_next(1));
		d.add_son(32, d.get_head()->get_next(1)->get_next(0));
		d.add_son(323322, d.get_head()->get_next(1)->get_next(0));
		d.add_son(331, d.get_head()->get_next(1)->get_next(0)->get_next(0));
		d.add_son(332, d.get_head()->get_next(1)->get_next(0)->get_next(0));
		d.add_son(333, d.get_head()->get_next(1)->get_next(0)->get_next(0));
		d.add_son(4, d.get_head());
		d.add_son(5, d.get_head());
		d.add_son(6, d.get_head());
		cout << "ilosc: " << d.size() << endl;
		d.print_pre();
		//d.remove_son(0, d.get_head()->get_next(1)->get_next(1));
		//cout << "ilosc: " << d.size() << endl;
	//	d.print();
		cout << "ilosc: " << d.size() << endl;
		//cout << "wysokosc: " << d.height(d.get_head()) << endl;
		d.print_post();
	}
	catch (string w)
	{
		cout << "Wyjatek: " << w << endl;
	}
	system("PAUSE");
}